/**
 * @license Copyright (c) 2003-2020, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */

/**
 * @module special-characters/specialcharactersarrows
 */

import Plugin from '@ckeditor/ckeditor5-core/src/plugin';

/**
 * A plugin that provides special characters for the "Greek" category.
 *
 *		ClassicEditor
 *			.create( {
 *				plugins: [ ..., SpecialCharacters, SpecialCharactersArrows ],
 *			} )
 *			.then( ... )
 *			.catch( ... );
 *
 * @extends module:core/plugin~Plugin
 */
export default class SpecialCharactersGreek extends Plugin {
	/**
	 * @inheritDoc
	 */
	init() {
		const editor = this.editor;
		const t = editor.t;

		editor.plugins.get( 'SpecialCharacters' ).addItems( 'Greek', [
			{ title: t( 'capital alpha' ), character: 'A' },
			{ title: t( 'capital beta' ), character: 'B' },
			{ title: t( 'capital gamma' ), character: 'Γ' },
			{ title: t( 'capital delta' ), character: 'Δ' },
			{ title: t( 'capital epsilon' ), character: 'E' },
			{ title: t( 'capital zeta' ), character: 'Z' },
			{ title: t( 'capital eta' ), character: 'H' },
			{ title: t( 'capital theta' ), character: 'Θ' },
			{ title: t( 'capital iota' ), character: 'I' },
			{ title: t( 'capital kappa' ), character: 'K' },
			{ title: t( 'capital lambda' ), character: 'Λ' },
			{ title: t( 'capital mu' ), character: 'M' },
			{ title: t( 'capital nu' ), character: 'N' },
			{ title: t( 'capital xi' ), character: 'Ξ' },
			{ title: t( 'capital omicron' ), character: 'O' },
			{ title: t( 'capital pi' ), character: 'Π' },
			{ title: t( 'capital rho' ), character: 'P' },
			{ title: t( 'capital sigma' ), character: 'Σ' },
			{ title: t( 'capital tau' ), character: 'T' },
			{ title: t( 'capital upsilon' ), character: 'Y' },
			{ title: t( 'capital phi' ), character: 'Φ' },
			{ title: t( 'capital chi' ), character: 'X' },
			{ title: t( 'capital psi' ), character: 'Ψ' },
			{ title: t( 'capital omega' ), character: 'Ω' },

			{ title: t( 'small alpha' ), character: 'α' },
			{ title: t( 'small beta' ), character: 'β' },
			{ title: t( 'small gamma' ), character: 'γ' },
			{ title: t( 'small delta' ), character: 'δ' },
			{ title: t( 'small epsilon' ), character: 'ε' },
			{ title: t( 'small zeta' ), character: 'ζ' },
			{ title: t( 'small eta' ), character: 'η' },
			{ title: t( 'small theta' ), character: 'θ' },
			{ title: t( 'small iota' ), character: 'ι' },
			{ title: t( 'small kappa' ), character: 'κ' },
			{ title: t( 'small lambda' ), character: 'λ' },
			{ title: t( 'small mu' ), character: 'μ' },
			{ title: t( 'small nu' ), character: 'ν' },
			{ title: t( 'small xi' ), character: 'ξ' },
			{ title: t( 'small omicron' ), character: 'o' },
			{ title: t( 'small pi' ), character: 'π' },
			{ title: t( 'small rho' ), character: 'ρ' },
			{ title: t( 'small sigma' ), character: 'σ' },
			{ title: t( 'small tau' ), character: 'τ' },
			{ title: t( 'small upsilon' ), character: 'υ' },
			{ title: t( 'small phi' ), character: 'φ' },
			{ title: t( 'small chi' ), character: 'χ' },
			{ title: t( 'small psi' ), character: 'ψ' },
			{ title: t( 'small omega' ), character: 'ω'}
			
		] );
	}
}
